<?php
/**
 * VoucherTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  VertisConstellation
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Documentação - API Constellation
 *
 * API Constellation
 *
 * OpenAPI spec version: V1.1
 * Contact: gpi@gpiti.com.br
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.21
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace VertisConstellation;

/**
 * VoucherTest Class Doc Comment
 *
 * @category    Class
 * @description Voucher
 * @package     VertisConstellation
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class VoucherTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "Voucher"
     */
    public function testVoucher()
    {
    }

    /**
     * Test attribute "id_voucher"
     */
    public function testPropertyIdVoucher()
    {
    }

    /**
     * Test attribute "id_unid_negoc"
     */
    public function testPropertyIdUnidNegoc()
    {
    }

    /**
     * Test attribute "id_unid_oper"
     */
    public function testPropertyIdUnidOper()
    {
    }

    /**
     * Test attribute "id_parceiro"
     */
    public function testPropertyIdParceiro()
    {
    }

    /**
     * Test attribute "cod_unid_oper"
     */
    public function testPropertyCodUnidOper()
    {
    }

    /**
     * Test attribute "cod_parceiro"
     */
    public function testPropertyCodParceiro()
    {
    }

    /**
     * Test attribute "nro_voucher"
     */
    public function testPropertyNroVoucher()
    {
    }

    /**
     * Test attribute "dth_ativacao"
     */
    public function testPropertyDthAtivacao()
    {
    }

    /**
     * Test attribute "ind_sit_voucher"
     */
    public function testPropertyIndSitVoucher()
    {
    }

    /**
     * Test attribute "dth_inclusao"
     */
    public function testPropertyDthInclusao()
    {
    }

    /**
     * Test attribute "dth_exclusao"
     */
    public function testPropertyDthExclusao()
    {
    }

    /**
     * Test attribute "ind_tipo_parceiro"
     */
    public function testPropertyIndTipoParceiro()
    {
    }

    /**
     * Test attribute "nom_parceiro"
     */
    public function testPropertyNomParceiro()
    {
    }

    /**
     * Test attribute "recordcount"
     */
    public function testPropertyRecordcount()
    {
    }
}
