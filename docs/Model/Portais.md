# Portais

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id_portal** | **int** | #field_definition# | 
**nom_portal** | **string** | #field_definition# | 
**url_portal** | **string** | #field_definition# | 
**app_ready** | **string** | #field_definition# | 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


