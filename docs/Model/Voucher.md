# Voucher

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id_voucher** | **int** | #field_definition# | 
**id_unid_negoc** | **int** | #field_definition# | 
**id_unid_oper** | **int** | #field_definition# | 
**id_parceiro** | **int** | #field_definition# | 
**cod_unid_oper** | **int** | #field_definition# | 
**cod_parceiro** | **int** | #field_definition# | 
**nro_voucher** | **string** | #field_definition# | 
**dth_ativacao** | **string** | #field_definition# | 
**ind_sit_voucher** | **string** | #field_definition# | 
**dth_inclusao** | **string** | #field_definition# | 
**dth_exclusao** | **string** | #field_definition# | 
**ind_tipo_parceiro** | **string** | #field_definition# | 
**nom_parceiro** | **string** |  | [optional] 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


