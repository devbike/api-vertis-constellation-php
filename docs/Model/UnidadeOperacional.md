# UnidadeOperacional

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id_unid_operacional** | **int** | #field_definition# | 
**cod_unid_negoc** | **int** | #field_definition# | 
**cod_unid_oper** | **int** | #field_definition# | 
**cnpj_unid_oper** | **string** | #field_definition# | 
**nom_unid_oper** | **string** | #field_definition# | 
**nom_fantasia** | **string** | #field_definition# | 
**nom_contato** | **string** | #field_definition# | 
**email_contato** | **string** | #field_definition# | 
**tel_contato** | **string** | #field_definition# | 
**ind_util_vertis** | **string** | #field_definition# | 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


