# VertisConstellation\MotoboyApi

All URIs are relative to *http://52.67.153.113:80*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createMotoboy**](MotoboyApi.md#createMotoboy) | **POST** /constellation-api/V1.1/motoboys | 
[**deleteMotoboy**](MotoboyApi.md#deleteMotoboy) | **DELETE** /constellation-api/V1.1/motoboys/{id} | 
[**getMotoboy**](MotoboyApi.md#getMotoboy) | **GET** /constellation-api/V1.1/motoboys/{id} | 
[**getMotoboys**](MotoboyApi.md#getMotoboys) | **GET** /constellation-api/V1.1/motoboys | 
[**updateMotoboy**](MotoboyApi.md#updateMotoboy) | **PUT** /constellation-api/V1.1/motoboys/{id} | 


# **createMotoboy**
> \VertisConstellation\Model\ModelMotoboy createMotoboy($motoboy)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConstellation\Api\MotoboyApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$motoboy = new \VertisConstellation\Model\Motoboy1(); // \VertisConstellation\Model\Motoboy1 | Objeto parc_motoboy

try {
    $result = $apiInstance->createMotoboy($motoboy);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MotoboyApi->createMotoboy: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **motoboy** | [**\VertisConstellation\Model\Motoboy1**](../Model/Motoboy1.md)| Objeto parc_motoboy |

### Return type

[**\VertisConstellation\Model\ModelMotoboy**](../Model/ModelMotoboy.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteMotoboy**
> \VertisConstellation\Model\ModelMotoboy deleteMotoboy($id)



Apaga o registro determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConstellation\Api\MotoboyApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID chave do Registro

try {
    $result = $apiInstance->deleteMotoboy($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MotoboyApi->deleteMotoboy: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID chave do Registro |

### Return type

[**\VertisConstellation\Model\ModelMotoboy**](../Model/ModelMotoboy.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getMotoboy**
> \VertisConstellation\Model\ModelMotoboy[] getMotoboy($id)



Retorna o motoboy selecionado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConstellation\Api\MotoboyApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID chave do regoistro

try {
    $result = $apiInstance->getMotoboy($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MotoboyApi->getMotoboy: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID chave do regoistro |

### Return type

[**\VertisConstellation\Model\ModelMotoboy[]**](../Model/ModelMotoboy.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getMotoboys**
> \VertisConstellation\Model\ModelMotoboy[] getMotoboys()



Retorna todos os motoboys cadastrados.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConstellation\Api\MotoboyApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getMotoboys();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MotoboyApi->getMotoboys: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConstellation\Model\ModelMotoboy[]**](../Model/ModelMotoboy.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateMotoboy**
> \VertisConstellation\Model\ModelMotoboy updateMotoboy($motoboy, $id)



Atualiza o registro determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConstellation\Api\MotoboyApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$motoboy = new \VertisConstellation\Model\Motoboy(); // \VertisConstellation\Model\Motoboy | Objeto parc_motoboy
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->updateMotoboy($motoboy, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MotoboyApi->updateMotoboy: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **motoboy** | [**\VertisConstellation\Model\Motoboy**](../Model/Motoboy.md)| Objeto parc_motoboy |
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConstellation\Model\ModelMotoboy**](../Model/ModelMotoboy.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

