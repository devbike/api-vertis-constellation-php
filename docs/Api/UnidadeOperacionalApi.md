# VertisConstellation\UnidadeOperacionalApi

All URIs are relative to *http://52.67.153.113:80*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createUnidadeOperacional**](UnidadeOperacionalApi.md#createUnidadeOperacional) | **POST** /constellation-api/V1.1/unidades-operacionais | 
[**deleteUnidadeOperacional**](UnidadeOperacionalApi.md#deleteUnidadeOperacional) | **DELETE** /constellation-api/V1.1/unidades-operacionais/{id} | 
[**getComboUnidadesOperacionais**](UnidadeOperacionalApi.md#getComboUnidadesOperacionais) | **GET** /constellation-api/V1.1/unidades-operacionais/combo | 
[**getUnidadeOperacional**](UnidadeOperacionalApi.md#getUnidadeOperacional) | **GET** /constellation-api/V1.1/unidades-operacionais/{id} | 
[**getUnidadesOperacionais**](UnidadeOperacionalApi.md#getUnidadesOperacionais) | **GET** /constellation-api/V1.1/unidades-operacionais | 
[**updateUnidadeOperacional**](UnidadeOperacionalApi.md#updateUnidadeOperacional) | **PUT** /constellation-api/V1.1/unidades-operacionais/{id} | 


# **createUnidadeOperacional**
> \VertisConstellation\Model\ModelUnidOper createUnidadeOperacional($unidade_operacional)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConstellation\Api\UnidadeOperacionalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$unidade_operacional = new \VertisConstellation\Model\UnidadeOperacional(); // \VertisConstellation\Model\UnidadeOperacional | Objeto parc_usuario

try {
    $result = $apiInstance->createUnidadeOperacional($unidade_operacional);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UnidadeOperacionalApi->createUnidadeOperacional: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **unidade_operacional** | [**\VertisConstellation\Model\UnidadeOperacional**](../Model/UnidadeOperacional.md)| Objeto parc_usuario |

### Return type

[**\VertisConstellation\Model\ModelUnidOper**](../Model/ModelUnidOper.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteUnidadeOperacional**
> \VertisConstellation\Model\ModelUnidOper deleteUnidadeOperacional($id)



Exclui o registro determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConstellation\Api\UnidadeOperacionalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->deleteUnidadeOperacional($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UnidadeOperacionalApi->deleteUnidadeOperacional: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConstellation\Model\ModelUnidOper**](../Model/ModelUnidOper.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getComboUnidadesOperacionais**
> \VertisConstellation\Model\ModelUnidOper[] getComboUnidadesOperacionais()



Retorna lista de unidades cadastradas.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConstellation\Api\UnidadeOperacionalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getComboUnidadesOperacionais();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UnidadeOperacionalApi->getComboUnidadesOperacionais: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConstellation\Model\ModelUnidOper[]**](../Model/ModelUnidOper.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getUnidadeOperacional**
> \VertisConstellation\Model\ModelUnidOper getUnidadeOperacional($id)



Retorna informações de um registro determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConstellation\Api\UnidadeOperacionalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->getUnidadeOperacional($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UnidadeOperacionalApi->getUnidadeOperacional: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConstellation\Model\ModelUnidOper**](../Model/ModelUnidOper.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getUnidadesOperacionais**
> \VertisConstellation\Model\ModelUnidOper[] getUnidadesOperacionais()



Retorna lista de unidades cadastradas.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConstellation\Api\UnidadeOperacionalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getUnidadesOperacionais();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UnidadeOperacionalApi->getUnidadesOperacionais: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConstellation\Model\ModelUnidOper[]**](../Model/ModelUnidOper.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateUnidadeOperacional**
> \VertisConstellation\Model\ModelUnidOper updateUnidadeOperacional($unidade_operacional, $id)



Atualiza o registro determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConstellation\Api\UnidadeOperacionalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$unidade_operacional = new \VertisConstellation\Model\UnidadeOperacional1(); // \VertisConstellation\Model\UnidadeOperacional1 | Objeto parc_Usuário
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->updateUnidadeOperacional($unidade_operacional, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UnidadeOperacionalApi->updateUnidadeOperacional: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **unidade_operacional** | [**\VertisConstellation\Model\UnidadeOperacional1**](../Model/UnidadeOperacional1.md)| Objeto parc_Usuário |
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConstellation\Model\ModelUnidOper**](../Model/ModelUnidOper.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

