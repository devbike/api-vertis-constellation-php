# VertisConstellation\BanhistaETosadorApi

All URIs are relative to *http://52.67.153.113:80*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createBanhistaTosador**](BanhistaETosadorApi.md#createBanhistaTosador) | **POST** /constellation-api/V1.1/parcsbanhotosa | 
[**deleteBanhistaTosador**](BanhistaETosadorApi.md#deleteBanhistaTosador) | **DELETE** /constellation-api/V1.1/parcsbanhotosa/{id} | 
[**getAllBanhistaTosador**](BanhistaETosadorApi.md#getAllBanhistaTosador) | **GET** /constellation-api/V1.1/parcsbanhotosa | 
[**getBanhistaTosador**](BanhistaETosadorApi.md#getBanhistaTosador) | **GET** /constellation-api/V1.1/parcsbanhotosa/{id} | 
[**updateBanhistaTosador**](BanhistaETosadorApi.md#updateBanhistaTosador) | **PUT** /constellation-api/V1.1/parcsbanhotosa/{id} | 


# **createBanhistaTosador**
> \VertisConstellation\Model\ModelParcBanhoTosa createBanhistaTosador($banhista_e_tosador)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConstellation\Api\BanhistaETosadorApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$banhista_e_tosador = new \VertisConstellation\Model\BanhistaETosador1(); // \VertisConstellation\Model\BanhistaETosador1 | Objeto parc_banhista_tosador

try {
    $result = $apiInstance->createBanhistaTosador($banhista_e_tosador);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BanhistaETosadorApi->createBanhistaTosador: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **banhista_e_tosador** | [**\VertisConstellation\Model\BanhistaETosador1**](../Model/BanhistaETosador1.md)| Objeto parc_banhista_tosador |

### Return type

[**\VertisConstellation\Model\ModelParcBanhoTosa**](../Model/ModelParcBanhoTosa.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteBanhistaTosador**
> \VertisConstellation\Model\ModelParcBanhoTosa deleteBanhistaTosador($id)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConstellation\Api\BanhistaETosadorApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Banhista/Tosador

try {
    $result = $apiInstance->deleteBanhistaTosador($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BanhistaETosadorApi->deleteBanhistaTosador: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Banhista/Tosador |

### Return type

[**\VertisConstellation\Model\ModelParcBanhoTosa**](../Model/ModelParcBanhoTosa.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAllBanhistaTosador**
> \VertisConstellation\Model\ModelParcBanhoTosa[] getAllBanhistaTosador()



Retorna todos os banhistas e tosadores.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConstellation\Api\BanhistaETosadorApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getAllBanhistaTosador();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BanhistaETosadorApi->getAllBanhistaTosador: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConstellation\Model\ModelParcBanhoTosa[]**](../Model/ModelParcBanhoTosa.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getBanhistaTosador**
> \VertisConstellation\Model\ModelParcBanhoTosa getBanhistaTosador($id)



Retorna informações de um Banhista/Tosador especificado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConstellation\Api\BanhistaETosadorApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Banhista/Tosador

try {
    $result = $apiInstance->getBanhistaTosador($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BanhistaETosadorApi->getBanhistaTosador: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Banhista/Tosador |

### Return type

[**\VertisConstellation\Model\ModelParcBanhoTosa**](../Model/ModelParcBanhoTosa.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateBanhistaTosador**
> \VertisConstellation\Model\ModelParcBanhoTosa updateBanhistaTosador($banhista_e_tosador, $id)



Atualiza o registro de Banhista/Tosador determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConstellation\Api\BanhistaETosadorApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$banhista_e_tosador = new \VertisConstellation\Model\BanhistaETosador(); // \VertisConstellation\Model\BanhistaETosador | Objeto parc_banhista_tosador
$id = 56; // int | ID do Banhista/Tosador

try {
    $result = $apiInstance->updateBanhistaTosador($banhista_e_tosador, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BanhistaETosadorApi->updateBanhistaTosador: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **banhista_e_tosador** | [**\VertisConstellation\Model\BanhistaETosador**](../Model/BanhistaETosador.md)| Objeto parc_banhista_tosador |
 **id** | **int**| ID do Banhista/Tosador |

### Return type

[**\VertisConstellation\Model\ModelParcBanhoTosa**](../Model/ModelParcBanhoTosa.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

