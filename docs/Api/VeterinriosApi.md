# VertisConstellation\VeterinriosApi

All URIs are relative to *http://52.67.153.113:80*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createVeterinario**](VeterinriosApi.md#createVeterinario) | **POST** /constellation-api/V1.1/veterinarios | 
[**deleteVeterinario**](VeterinriosApi.md#deleteVeterinario) | **DELETE** /constellation-api/V1.1/veterinarios/{id} | 
[**getVeterinario**](VeterinriosApi.md#getVeterinario) | **GET** /constellation-api/V1.1/veterinarios/{id} | 
[**getVeterinarios**](VeterinriosApi.md#getVeterinarios) | **GET** /constellation-api/V1.1/veterinarios | 
[**updateVeterinario**](VeterinriosApi.md#updateVeterinario) | **PUT** /constellation-api/V1.1/veterinarios/{id} | 


# **createVeterinario**
> \VertisConstellation\Model\ModelVeterinarios createVeterinario($veterinrios)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConstellation\Api\VeterinriosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$veterinrios = new \VertisConstellation\Model\Veterinrios1(); // \VertisConstellation\Model\Veterinrios1 | Objeto parc_veterinario

try {
    $result = $apiInstance->createVeterinario($veterinrios);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VeterinriosApi->createVeterinario: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **veterinrios** | [**\VertisConstellation\Model\Veterinrios1**](../Model/Veterinrios1.md)| Objeto parc_veterinario |

### Return type

[**\VertisConstellation\Model\ModelVeterinarios**](../Model/ModelVeterinarios.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteVeterinario**
> \VertisConstellation\Model\ModelVeterinarios deleteVeterinario($id)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConstellation\Api\VeterinriosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Veterinários

try {
    $result = $apiInstance->deleteVeterinario($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VeterinriosApi->deleteVeterinario: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Veterinários |

### Return type

[**\VertisConstellation\Model\ModelVeterinarios**](../Model/ModelVeterinarios.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getVeterinario**
> \VertisConstellation\Model\ModelVeterinarios getVeterinario($id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConstellation\Api\VeterinriosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Veterinários

try {
    $result = $apiInstance->getVeterinario($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VeterinriosApi->getVeterinario: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Veterinários |

### Return type

[**\VertisConstellation\Model\ModelVeterinarios**](../Model/ModelVeterinarios.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getVeterinarios**
> \VertisConstellation\Model\ModelVeterinarios[] getVeterinarios()



Retorna registros do objeto parc_veterinario

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConstellation\Api\VeterinriosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getVeterinarios();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VeterinriosApi->getVeterinarios: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConstellation\Model\ModelVeterinarios[]**](../Model/ModelVeterinarios.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateVeterinario**
> \VertisConstellation\Model\ModelVeterinarios updateVeterinario($veterinrios, $id)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConstellation\Api\VeterinriosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$veterinrios = new \VertisConstellation\Model\Veterinrios(); // \VertisConstellation\Model\Veterinrios | Objeto parc_veterinario
$id = 56; // int | ID do Veterinários

try {
    $result = $apiInstance->updateVeterinario($veterinrios, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VeterinriosApi->updateVeterinario: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **veterinrios** | [**\VertisConstellation\Model\Veterinrios**](../Model/Veterinrios.md)| Objeto parc_veterinario |
 **id** | **int**| ID do Veterinários |

### Return type

[**\VertisConstellation\Model\ModelVeterinarios**](../Model/ModelVeterinarios.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

