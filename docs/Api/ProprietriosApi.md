# VertisConstellation\ProprietriosApi

All URIs are relative to *http://52.67.153.113:80*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createProprietarios**](ProprietriosApi.md#createProprietarios) | **POST** /constellation-api/V1.1/proprietarios | 
[**deleteProprietarios**](ProprietriosApi.md#deleteProprietarios) | **DELETE** /constellation-api/V1.1/proprietarios/{id} | 
[**getProprietarios**](ProprietriosApi.md#getProprietarios) | **GET** /constellation-api/V1.1/proprietarios/{id} | 
[**getProprietarioss**](ProprietriosApi.md#getProprietarioss) | **GET** /constellation-api/V1.1/proprietarios | 
[**updateProprietarios**](ProprietriosApi.md#updateProprietarios) | **PUT** /constellation-api/V1.1/proprietarios/{id} | 


# **createProprietarios**
> \VertisConstellation\Model\ModelProprietarios createProprietarios($proprietrios)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConstellation\Api\ProprietriosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$proprietrios = new \VertisConstellation\Model\Proprietrios1(); // \VertisConstellation\Model\Proprietrios1 | Objeto parc_proprietario

try {
    $result = $apiInstance->createProprietarios($proprietrios);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProprietriosApi->createProprietarios: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **proprietrios** | [**\VertisConstellation\Model\Proprietrios1**](../Model/Proprietrios1.md)| Objeto parc_proprietario |

### Return type

[**\VertisConstellation\Model\ModelProprietarios**](../Model/ModelProprietarios.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteProprietarios**
> \VertisConstellation\Model\ModelProprietarios deleteProprietarios($id)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConstellation\Api\ProprietriosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Proprietarios

try {
    $result = $apiInstance->deleteProprietarios($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProprietriosApi->deleteProprietarios: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Proprietarios |

### Return type

[**\VertisConstellation\Model\ModelProprietarios**](../Model/ModelProprietarios.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getProprietarios**
> \VertisConstellation\Model\ModelProprietarios getProprietarios($id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConstellation\Api\ProprietriosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Proprietarios

try {
    $result = $apiInstance->getProprietarios($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProprietriosApi->getProprietarios: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Proprietarios |

### Return type

[**\VertisConstellation\Model\ModelProprietarios**](../Model/ModelProprietarios.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getProprietarioss**
> \VertisConstellation\Model\ModelProprietarios[] getProprietarioss()



Retorna registros do objeto parc_proprietario

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConstellation\Api\ProprietriosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getProprietarioss();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProprietriosApi->getProprietarioss: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConstellation\Model\ModelProprietarios[]**](../Model/ModelProprietarios.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateProprietarios**
> \VertisConstellation\Model\ModelProprietarios updateProprietarios($proprietrios, $id)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConstellation\Api\ProprietriosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$proprietrios = new \VertisConstellation\Model\Proprietrios(); // \VertisConstellation\Model\Proprietrios | Objeto parc_proprietario
$id = 56; // int | ID do Proprietarios

try {
    $result = $apiInstance->updateProprietarios($proprietrios, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProprietriosApi->updateProprietarios: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **proprietrios** | [**\VertisConstellation\Model\Proprietrios**](../Model/Proprietrios.md)| Objeto parc_proprietario |
 **id** | **int**| ID do Proprietarios |

### Return type

[**\VertisConstellation\Model\ModelProprietarios**](../Model/ModelProprietarios.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

