# VertisConstellation\ControllerUsuarioTControllerUsuarioApi

All URIs are relative to *http://52.67.153.113:80*

Method | HTTP request | Description
------------- | ------------- | -------------
[**constellationApiV11UsuariosGet**](ControllerUsuarioTControllerUsuarioApi.md#constellationApiV11UsuariosGet) | **GET** /constellation-api/V1.1/usuarios | 
[**constellationApiV11UsuariosIdDelete**](ControllerUsuarioTControllerUsuarioApi.md#constellationApiV11UsuariosIdDelete) | **DELETE** /constellation-api/V1.1/usuarios/{id} | 
[**constellationApiV11UsuariosIdGet**](ControllerUsuarioTControllerUsuarioApi.md#constellationApiV11UsuariosIdGet) | **GET** /constellation-api/V1.1/usuarios/{id} | 
[**constellationApiV11UsuariosIdPut**](ControllerUsuarioTControllerUsuarioApi.md#constellationApiV11UsuariosIdPut) | **PUT** /constellation-api/V1.1/usuarios/{id} | 
[**constellationApiV11UsuariosPost**](ControllerUsuarioTControllerUsuarioApi.md#constellationApiV11UsuariosPost) | **POST** /constellation-api/V1.1/usuarios | 


# **constellationApiV11UsuariosGet**
> constellationApiV11UsuariosGet()



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConstellation\Api\ControllerUsuarioTControllerUsuarioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $apiInstance->constellationApiV11UsuariosGet();
} catch (Exception $e) {
    echo 'Exception when calling ControllerUsuarioTControllerUsuarioApi->constellationApiV11UsuariosGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **constellationApiV11UsuariosIdDelete**
> constellationApiV11UsuariosIdDelete($id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConstellation\Api\ControllerUsuarioTControllerUsuarioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | 

try {
    $apiInstance->constellationApiV11UsuariosIdDelete($id);
} catch (Exception $e) {
    echo 'Exception when calling ControllerUsuarioTControllerUsuarioApi->constellationApiV11UsuariosIdDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **constellationApiV11UsuariosIdGet**
> constellationApiV11UsuariosIdGet($id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConstellation\Api\ControllerUsuarioTControllerUsuarioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | 

try {
    $apiInstance->constellationApiV11UsuariosIdGet($id);
} catch (Exception $e) {
    echo 'Exception when calling ControllerUsuarioTControllerUsuarioApi->constellationApiV11UsuariosIdGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **constellationApiV11UsuariosIdPut**
> constellationApiV11UsuariosIdPut($id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConstellation\Api\ControllerUsuarioTControllerUsuarioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | 

try {
    $apiInstance->constellationApiV11UsuariosIdPut($id);
} catch (Exception $e) {
    echo 'Exception when calling ControllerUsuarioTControllerUsuarioApi->constellationApiV11UsuariosIdPut: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **constellationApiV11UsuariosPost**
> constellationApiV11UsuariosPost()



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConstellation\Api\ControllerUsuarioTControllerUsuarioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $apiInstance->constellationApiV11UsuariosPost();
} catch (Exception $e) {
    echo 'Exception when calling ControllerUsuarioTControllerUsuarioApi->constellationApiV11UsuariosPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

