# VertisConstellation\VoucherApi

All URIs are relative to *http://52.67.153.113:80*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createVoucher**](VoucherApi.md#createVoucher) | **POST** /constellation-api/V1.1/voucher | 
[**createVoucherByLote**](VoucherApi.md#createVoucherByLote) | **POST** /constellation-api/V1.1/voucher-lote | 
[**deleteVoucher**](VoucherApi.md#deleteVoucher) | **DELETE** /constellation-api/V1.1/vouchers/{id} | 
[**getVoucher**](VoucherApi.md#getVoucher) | **GET** /constellation-api/V1.1/vouchers/{id} | 
[**getVouchers**](VoucherApi.md#getVouchers) | **GET** /constellation-api/V1.1/vouchers | 
[**iniciaAtivacaoVoucher**](VoucherApi.md#iniciaAtivacaoVoucher) | **PUT** /constellation-api/V1.1/vouchers-ativar | 
[**registrarUsoVoucher**](VoucherApi.md#registrarUsoVoucher) | **PUT** /constellation-api/V1.1/vouchers-finaliza-ativacao | 
[**updateVoucher**](VoucherApi.md#updateVoucher) | **PUT** /constellation-api/V1.1/vouchers/{id} | 
[**verificaVoucher**](VoucherApi.md#verificaVoucher) | **GET** /constellation-api/V1.1/vouchers-validar | 


# **createVoucher**
> \VertisConstellation\Model\ModelVoucher createVoucher($voucher)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConstellation\Api\VoucherApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$voucher = new \VertisConstellation\Model\Voucher1(); // \VertisConstellation\Model\Voucher1 | Objeto unid_neg_voucher

try {
    $result = $apiInstance->createVoucher($voucher);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VoucherApi->createVoucher: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **voucher** | [**\VertisConstellation\Model\Voucher1**](../Model/Voucher1.md)| Objeto unid_neg_voucher |

### Return type

[**\VertisConstellation\Model\ModelVoucher**](../Model/ModelVoucher.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createVoucherByLote**
> \VertisConstellation\Model\ModelVoucher createVoucherByLote($voucher)



Insere um lote de vouchers.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConstellation\Api\VoucherApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$voucher = new \VertisConstellation\Model\Voucher2(); // \VertisConstellation\Model\Voucher2 | Objeto unid_neg_voucher

try {
    $result = $apiInstance->createVoucherByLote($voucher);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VoucherApi->createVoucherByLote: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **voucher** | [**\VertisConstellation\Model\Voucher2**](../Model/Voucher2.md)| Objeto unid_neg_voucher |

### Return type

[**\VertisConstellation\Model\ModelVoucher**](../Model/ModelVoucher.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteVoucher**
> \VertisConstellation\Model\ModelVoucher deleteVoucher($id)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConstellation\Api\VoucherApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Voucher

try {
    $result = $apiInstance->deleteVoucher($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VoucherApi->deleteVoucher: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Voucher |

### Return type

[**\VertisConstellation\Model\ModelVoucher**](../Model/ModelVoucher.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getVoucher**
> \VertisConstellation\Model\ModelVoucher getVoucher($id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConstellation\Api\VoucherApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Voucher

try {
    $result = $apiInstance->getVoucher($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VoucherApi->getVoucher: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Voucher |

### Return type

[**\VertisConstellation\Model\ModelVoucher**](../Model/ModelVoucher.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getVouchers**
> \VertisConstellation\Model\ModelVoucher[] getVouchers()



Retorna registros do objeto unid_neg_voucher

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConstellation\Api\VoucherApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getVouchers();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VoucherApi->getVouchers: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConstellation\Model\ModelVoucher[]**](../Model/ModelVoucher.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **iniciaAtivacaoVoucher**
> \VertisConstellation\Model\ModelVoucher iniciaAtivacaoVoucher($id)



Registra o inicio da ativação do voucher em um app.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConstellation\Api\VoucherApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | Número do Voucher

try {
    $result = $apiInstance->iniciaAtivacaoVoucher($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VoucherApi->iniciaAtivacaoVoucher: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Número do Voucher |

### Return type

[**\VertisConstellation\Model\ModelVoucher**](../Model/ModelVoucher.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **registrarUsoVoucher**
> \VertisConstellation\Model\ModelVoucher registrarUsoVoucher($id)



Registra a instalação do voucher.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConstellation\Api\VoucherApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | Número do Voucher

try {
    $result = $apiInstance->registrarUsoVoucher($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VoucherApi->registrarUsoVoucher: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Número do Voucher |

### Return type

[**\VertisConstellation\Model\ModelVoucher**](../Model/ModelVoucher.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateVoucher**
> \VertisConstellation\Model\ModelVoucher updateVoucher($voucher, $id)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConstellation\Api\VoucherApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$voucher = new \VertisConstellation\Model\Voucher(); // \VertisConstellation\Model\Voucher | Objeto unid_neg_voucher
$id = 56; // int | ID do Voucher

try {
    $result = $apiInstance->updateVoucher($voucher, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VoucherApi->updateVoucher: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **voucher** | [**\VertisConstellation\Model\Voucher**](../Model/Voucher.md)| Objeto unid_neg_voucher |
 **id** | **int**| ID do Voucher |

### Return type

[**\VertisConstellation\Model\ModelVoucher**](../Model/ModelVoucher.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **verificaVoucher**
> \VertisConstellation\Model\ModelVoucher verificaVoucher($ref)



Verifica se o voucher está ativo.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConstellation\Api\VoucherApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$ref = "ref_example"; // string | Número do Voucher

try {
    $result = $apiInstance->verificaVoucher($ref);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VoucherApi->verificaVoucher: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ref** | **string**| Número do Voucher |

### Return type

[**\VertisConstellation\Model\ModelVoucher**](../Model/ModelVoucher.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

