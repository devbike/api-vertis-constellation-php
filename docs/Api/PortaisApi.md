# VertisConstellation\PortaisApi

All URIs are relative to *http://52.67.153.113:80*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createPortal**](PortaisApi.md#createPortal) | **POST** /constellation-api/V1.1/portais | 
[**deletePortal**](PortaisApi.md#deletePortal) | **DELETE** /constellation-api/V1.1/portais/{id} | 
[**getPortal**](PortaisApi.md#getPortal) | **GET** /constellation-api/V1.1/portais/{id} | 
[**getPortals**](PortaisApi.md#getPortals) | **GET** /constellation-api/V1.1/portais | 
[**updatePortal**](PortaisApi.md#updatePortal) | **PUT** /constellation-api/V1.1/portais/{id} | 


# **createPortal**
> \VertisConstellation\Model\ModelPortais createPortal($portais)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConstellation\Api\PortaisApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$portais = new \VertisConstellation\Model\Portais1(); // \VertisConstellation\Model\Portais1 | Objeto portais

try {
    $result = $apiInstance->createPortal($portais);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PortaisApi->createPortal: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **portais** | [**\VertisConstellation\Model\Portais1**](../Model/Portais1.md)| Objeto portais |

### Return type

[**\VertisConstellation\Model\ModelPortais**](../Model/ModelPortais.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deletePortal**
> \VertisConstellation\Model\ModelPortais deletePortal($id)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConstellation\Api\PortaisApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Portais

try {
    $result = $apiInstance->deletePortal($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PortaisApi->deletePortal: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Portais |

### Return type

[**\VertisConstellation\Model\ModelPortais**](../Model/ModelPortais.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getPortal**
> \VertisConstellation\Model\ModelPortais getPortal($id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConstellation\Api\PortaisApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | IDentificador do Portal

try {
    $result = $apiInstance->getPortal($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PortaisApi->getPortal: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| IDentificador do Portal |

### Return type

[**\VertisConstellation\Model\ModelPortais**](../Model/ModelPortais.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getPortals**
> \VertisConstellation\Model\ModelPortais[] getPortals()



Retorna registros do objeto portais

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConstellation\Api\PortaisApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getPortals();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PortaisApi->getPortals: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConstellation\Model\ModelPortais[]**](../Model/ModelPortais.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updatePortal**
> \VertisConstellation\Model\ModelPortais updatePortal($portais, $id)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConstellation\Api\PortaisApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$portais = new \VertisConstellation\Model\Portais(); // \VertisConstellation\Model\Portais | Objeto portais
$id = 56; // int | ID do Portais

try {
    $result = $apiInstance->updatePortal($portais, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PortaisApi->updatePortal: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **portais** | [**\VertisConstellation\Model\Portais**](../Model/Portais.md)| Objeto portais |
 **id** | **int**| ID do Portais |

### Return type

[**\VertisConstellation\Model\ModelPortais**](../Model/ModelPortais.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

