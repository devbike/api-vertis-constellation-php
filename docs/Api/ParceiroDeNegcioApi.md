# VertisConstellation\ParceiroDeNegcioApi

All URIs are relative to *http://52.67.153.113:80*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createParceiroNegocio**](ParceiroDeNegcioApi.md#createParceiroNegocio) | **POST** /constellation-api/V1.1/parceironeg | 
[**createParceiroNegocioByLote**](ParceiroDeNegcioApi.md#createParceiroNegocioByLote) | **POST** /constellation-api/V1.1/parceironeg-lote | 
[**deleteParceiroNegocio**](ParceiroDeNegcioApi.md#deleteParceiroNegocio) | **DELETE** /constellation-api/V1.1/parceironeg/{id} | 
[**getParceiroNegocio**](ParceiroDeNegcioApi.md#getParceiroNegocio) | **GET** /constellation-api/V1.1/parceironeg/{id} | 
[**getParceiroNegocioByCod**](ParceiroDeNegcioApi.md#getParceiroNegocioByCod) | **GET** /constellation-api/V1.1/parceironeg/codigo/{id} | 
[**getParceiroNegocios**](ParceiroDeNegcioApi.md#getParceiroNegocios) | **GET** /constellation-api/V1.1/parceironeg | 
[**updateParceiroNegocio**](ParceiroDeNegcioApi.md#updateParceiroNegocio) | **PUT** /constellation-api/V1.1/parceironeg/{id} | 


# **createParceiroNegocio**
> \VertisConstellation\Model\ParceiroNegocio createParceiroNegocio($parceiro_de_negcio)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConstellation\Api\ParceiroDeNegcioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$parceiro_de_negcio = new \VertisConstellation\Model\ParceiroDeNegcio(); // \VertisConstellation\Model\ParceiroDeNegcio | Objeto parceiro_neg

try {
    $result = $apiInstance->createParceiroNegocio($parceiro_de_negcio);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ParceiroDeNegcioApi->createParceiroNegocio: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **parceiro_de_negcio** | [**\VertisConstellation\Model\ParceiroDeNegcio**](../Model/ParceiroDeNegcio.md)| Objeto parceiro_neg |

### Return type

[**\VertisConstellation\Model\ParceiroNegocio**](../Model/ParceiroNegocio.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createParceiroNegocioByLote**
> \VertisConstellation\Model\ParceiroNegocio createParceiroNegocioByLote($parceiro_de_negcio)



Insere um lote de parceiros.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConstellation\Api\ParceiroDeNegcioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$parceiro_de_negcio = new \VertisConstellation\Model\ParceiroDeNegcio2(); // \VertisConstellation\Model\ParceiroDeNegcio2 | Objeto parceiro_neg

try {
    $result = $apiInstance->createParceiroNegocioByLote($parceiro_de_negcio);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ParceiroDeNegcioApi->createParceiroNegocioByLote: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **parceiro_de_negcio** | [**\VertisConstellation\Model\ParceiroDeNegcio2**](../Model/ParceiroDeNegcio2.md)| Objeto parceiro_neg |

### Return type

[**\VertisConstellation\Model\ParceiroNegocio**](../Model/ParceiroNegocio.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteParceiroNegocio**
> \VertisConstellation\Model\ParceiroNegocio deleteParceiroNegocio($id)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConstellation\Api\ParceiroDeNegcioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Parceiro de Negócio

try {
    $result = $apiInstance->deleteParceiroNegocio($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ParceiroDeNegcioApi->deleteParceiroNegocio: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Parceiro de Negócio |

### Return type

[**\VertisConstellation\Model\ParceiroNegocio**](../Model/ParceiroNegocio.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getParceiroNegocio**
> \VertisConstellation\Model\ParceiroNegocio getParceiroNegocio($id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConstellation\Api\ParceiroDeNegcioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Parceiro de Negócio

try {
    $result = $apiInstance->getParceiroNegocio($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ParceiroDeNegcioApi->getParceiroNegocio: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Parceiro de Negócio |

### Return type

[**\VertisConstellation\Model\ParceiroNegocio**](../Model/ParceiroNegocio.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getParceiroNegocioByCod**
> \VertisConstellation\Model\ParceiroNegocio getParceiroNegocioByCod($id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConstellation\Api\ParceiroDeNegcioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Parceiro de Negócio

try {
    $result = $apiInstance->getParceiroNegocioByCod($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ParceiroDeNegcioApi->getParceiroNegocioByCod: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Parceiro de Negócio |

### Return type

[**\VertisConstellation\Model\ParceiroNegocio**](../Model/ParceiroNegocio.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getParceiroNegocios**
> \VertisConstellation\Model\ParceiroNegocio[] getParceiroNegocios()



Retorna registros do objeto parceiro_neg

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConstellation\Api\ParceiroDeNegcioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getParceiroNegocios();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ParceiroDeNegcioApi->getParceiroNegocios: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConstellation\Model\ParceiroNegocio[]**](../Model/ParceiroNegocio.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateParceiroNegocio**
> \VertisConstellation\Model\ParceiroNegocio updateParceiroNegocio($parceiro_de_negcio, $id)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConstellation\Api\ParceiroDeNegcioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$parceiro_de_negcio = new \VertisConstellation\Model\ParceiroDeNegcio1(); // \VertisConstellation\Model\ParceiroDeNegcio1 | Objeto parceiro_neg
$id = 56; // int | ID do Parceiro de Negócio

try {
    $result = $apiInstance->updateParceiroNegocio($parceiro_de_negcio, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ParceiroDeNegcioApi->updateParceiroNegocio: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **parceiro_de_negcio** | [**\VertisConstellation\Model\ParceiroDeNegcio1**](../Model/ParceiroDeNegcio1.md)| Objeto parceiro_neg |
 **id** | **int**| ID do Parceiro de Negócio |

### Return type

[**\VertisConstellation\Model\ParceiroNegocio**](../Model/ParceiroNegocio.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

